/**
 * massmove - A program to easily move/rename all files in a directory
 * v1.1 - 2019.05.16
 * Copyright (C) 2019 Sebastian LaVine <seblavine@outlook.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>. 
 */

#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char **argv, const char **environ)
{
    const char *path; // path of the user 
    for (int i = 0; environ[i] != NULL; i++)
    {
        if (strncmp(environ[i], "PWD", 3) == 0)
        {
            path = &(environ[i][4]);
        }
    }
    
    const char *dirname;

    DIR *dirp;
    struct dirent *dp;
    
    if (argc < 2)
    {
        dirname = path;
    } else
    {
        dirname = argv[1];
    }
   
    dirp = opendir(dirname);
    if (dirp == NULL)
    {
        puts("Error opening directory");
        return 1;
    }
    
    char editing_file_name[] = "/tmp/massmoveXXXXXX"; // Remember to change editor_command length line 31

    int edit_fd;
    if ((edit_fd = mkstemp(editing_file_name)) == -1)
    {
        puts("Error creating edit file");
        return 1;
    }
    
    const char *DASHLINE = "-----\n";
    int file_amt = 0;
    char **original_filenames = NULL;
    char **new_filenames = NULL;
    char **tmp_filenames;
    FILE *edit_fp = fdopen(edit_fd, "w+");

    fprintf(edit_fp, "massmove - Copyright 2019 Sebastian LaVine <seblavine@outlook.com>\nmassmove is free software, licensed under the GNU GPLv3+.\n"
            "On each line, edit the names of the files you want to move/rename.\n"
            "Directory: %s\n%s", dirname, DASHLINE);

    while ((dp = readdir(dirp)) != NULL) // loops through all files in directory, writing to file and to original_filenames
    { 
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
        {
            continue; // don't allow the user to move current/prev directory
        }

        fprintf(edit_fp, "%s\n", dp->d_name); // filename

        tmp_filenames = realloc(original_filenames, ++file_amt * sizeof(char *));
        if (tmp_filenames == NULL)
        {
            puts("Error allocating space for file names");
            free(original_filenames);
            return 1;
        }
        original_filenames = tmp_filenames;
        original_filenames[file_amt - 1] = dp->d_name;
    }

    // Sets the command to be used to open the file
    const char *EDITOR = "sensible-editor ";
    char editor_command[strlen(EDITOR) + strlen(editing_file_name) + 1];
    sprintf(editor_command, "%s%s", EDITOR, editing_file_name);

    rewind(edit_fp); // beginning of file

    // Open file in editor for user
    system(editor_command);

    /* Loops through file, reading lines until we find the dashes that separate
     * the header from file information. We don't care about how long it is.
     */
    char *looking_for_dashes = NULL; // holds potential dash lines
    size_t getline_tmp_len = 0; // length of looking_for_dashes
    for (;;)
    {
        getline(&looking_for_dashes, &getline_tmp_len, edit_fp);
        if (strcmp(looking_for_dashes, DASHLINE) == 0)
        {
            break;
        }
    }
    free(looking_for_dashes);

    new_filenames = malloc(file_amt * sizeof(char *));
    memset(new_filenames, 0, file_amt * sizeof(char *));

    puts("The following changes have been entered:");
    for (int i = 0; i < file_amt; i++)
    {
        getline_tmp_len = 0; // reusing from looking_for_dashes
        getline(&(new_filenames[i]), &getline_tmp_len, edit_fp);

        // getline ends line in newline; replace that with a nullchar
        new_filenames[i][strlen(new_filenames[i]) - 1] = '\0';

        printf("\"%s\" -> \"%s\"\n", original_filenames[i], new_filenames[i]);
    }

    // fputs b/c I don't want a newline at the end, like in puts()
    fputs("Do you confirm these changes? [y/n] ", stdout);

    if (getchar() == 'y')
    {
        for (int i = 0; i < file_amt; i++)
        {
        const size_t MVLEN = 1 + // null-terminator
            (strlen("mv ////\"\"\"\"") + // all extra chars
             strlen(original_filenames[i]) +
             strlen(new_filenames[i]) +
             strlen(path) * 2 +
             strlen(dirname) * 2) * sizeof(char);

            // if there is no change...
            if (strcmp(original_filenames[i], new_filenames[i]) == 0)
            {
                continue; // ...don't do anything
            }
            
            // build the mv command
            char *mv = malloc(MVLEN * sizeof(char));
            memset(mv, '\0', MVLEN);
            sprintf(mv, "mv \"%s/", path);
            if (dirname != path)
            {
                // add strlen to print to end of command
                sprintf(mv + strlen(mv), "%s/", dirname);
            }
            sprintf(mv + strlen(mv), "%s\" \"%s/", original_filenames[i],
                    path);
            if (dirname != path)
            {
                sprintf(mv + strlen(mv), "%s/", dirname);
            }
            sprintf(mv + strlen(mv), "%s\"", new_filenames[i]);

            // mv the file
            system(mv);
        }
    } else
    {
        puts("Aborted.");
    }
    
    free(original_filenames);
    free(new_filenames);
    remove(editing_file_name);
    closedir(dirp);
}
